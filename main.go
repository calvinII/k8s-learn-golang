package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"

    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	kubernetes "k8s.io/client-go/kubernetes"
	clientcmd "k8s.io/client-go/tools/clientcmd"
)

func connectToK8s() *kubernetes.Clientset {
	home, exists := os.LookupEnv("HOME")
	if ! exists {
		log.Panicln("can't find homedir")
		os.Exit(1)
	}
	configPath := filepath.Join(home, ".kube", "config")
	config, err := clientcmd.BuildConfigFromFlags("", configPath)
    if err != nil {
        log.Panicln("failed to create K8s config")
    }
    clientset, err := kubernetes.NewForConfig(config)
    if err != nil {
        log.Panicln("Failed to create K8s clientset")
    }

    return clientset

}

// func launchK8sJob(clientset *kubernetes.Clientset, jobName *string, image *string, cmd *string) {
// 	jobs := clientset.BatchV1().Jobs("default")
//     var backOffLimit int32 = 0

//     jobSpec := &batchv1.Job{
//         ObjectMeta: metav1.ObjectMeta{
//             Name:      *jobName,
//             Namespace: "default",
//         },
//         Spec: batchv1.JobSpec{
//             Template: v1.PodTemplateSpec{
//                 Spec: v1.PodSpec{
//                     Containers: []v1.Container{
//                         {
//                             Name:    *jobName,
//                             Image:   *image,
//                             Command: strings.Split(*cmd, " "),
//                         },
//                     },
//                     RestartPolicy: v1.RestartPolicyNever,
//                 },
//             },
//             BackoffLimit: &backOffLimit,
//         },
//     }

//     _, err := jobs.Create(context.TODO(), jobSpec, metav1.CreateOptions{})
//     if err != nil {
//         log.Fatalln("Failed to create K8s job.")
//     }

//     //print job details
//     log.Println("Created K8s job successfully")

// }

func main() {
    // jobName := flag.String("jobname", "test-job", "The name of the job")
    // containerImage := flag.String("image", "ubuntu:latest", "Name of the container image")
    // entryCommand := flag.String("command", "ls", "The command to run inside the container")
    // flag.Parse()
	clientset := connectToK8s()
	pods, err := clientset.CoreV1().Pods("monitoring").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		log.Panicln("Error:", err)
		os.Exit(1)
	}
	for _, pod := range pods.Items {
		fmt.Println(pod.Name)
	}

	// launchK8sJob(clientset, jobName, containerImage, entryCommand)

}
